Next
====

Mit `next` geht es direkt weiter zum nächsten State.

Ein State mit `next` action kann keine listen actions enthalten, da der State gewechselt wird, bevor die listener abgefragt werden können.

Die `next` action sollte die letzte action im State sein. Folgende actions werden nicht mehr ausgeführt.