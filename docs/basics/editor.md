Der Editor
==========

Hier wird dein Game zusammengebastelt

State
------

Ein State ist ein Zustand in dem sich dein level zu einem bestimmten Zeitpunkt und bis es zum nächsten state wechselt befindet.

### Benennung
### Kommentare



Action
------

Actions sind die kleinsten Bausteine für jedes adaptor:ex level. Hier passieren die wirklich wichtigen Dinge.

Jeder [State](../basics/editor#state) enthält eine oder mehrere actions.

Finde in der [Actions Referenz](../actions) mehr über die verschiedenen actions heraus.