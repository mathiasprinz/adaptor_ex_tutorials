# Storytelling mit Telegram und adaptor eX

adaptor:ex ist Teil des Projekts MACHINA COMMONS und wird im Programm "Digitale Entwicklung im Kulturbereich" der Berliner Senatsverwaltung für Kultur und Europa gefördert.

adaptor:ex ist eine open-source Software Engine, die es ermöglicht, interaktive Theatergames und -installationen umzusetzen. Ein Teilbereich davon sind inter(re)aktive Performances innerhalb des Messaging Services "Telegram".

## Grundprinzipien

adaptor:ex kann zu großen Teilen mit einem Browser (wir empfehlen Chromium/Chrome oder Firefox) bedient werden.
adaptor:ex organisiert die Theatergames/Performances/Show/Installationen in unterschiedlichen GAMES.
GAMES bestehen aus LEVELS.

Die LEVEL bestimmen, wie mit eingehenden Nachrichten umgegangen wird, und organisieren das in sogenannten STATES, die wiederum aus ACTIONS bestehen.

<img src="./assets/actions4-2.png" alt="screenshot" width="300px"/>

So sieht ein STATE aus. Die farbigen Blöcke innerhalb nennen wir ACTIONS.<br><br>
Ein LEVEL, das gerade bespielt wird / aktiv ist, nennen wir SESSION. Du kannst dir eine SESSION vorstellen wie eine Aufführung im Theater. Das LEVEL verhält sich dann zu der SESSION ein bisschen wie das Regiebuch und der Dramentext zur Aufführung.

Wir arbeiten in diesem Tutorial innerhalb von einem LEVEL und bauen ein Telegram-basiertes Chatadventure.

<div class="page"/>

## Schritt für Schritt

### Getting started
1. https://machinacommons.world/adaptor/#/game?game=WORKSHOP ist unser Workshop-Server mit einem bereits angelegten Theatergame namens "WORKSHOP". Klick auf das Level (blauer Button), das du bearbeiten möchtest …     

<img src="./assets/levelview.png" alt="screenshot"/>

2. … um in den Level Editor zu gelangen:

<img src="./assets/leveleditor-1.png" alt="screenshot leveleditor"/>

3.  Im Level Editor siehst du zwei verschiedene Bereiche: 
    
    a) Die "Stage" ist der größte Bereich im Leveleditor. Hier organisierst und verknüpfst du die "states" deines Games. 
    Du siehst bereits die zwei states, die es in jedem Level geben muss: START und QUIT.
    Zu Beginn des Levels landet die Spieler:in später immer im START state, und das Level endet erst, wenn sie den QUIT state erreicht.
    b) Links findest du die TOOLBAR. Alles, was du hier findest, kannst du mit der Maus  direkt auf die Bühne ziehen (via drag and drop).
    Die Toolbar gliedert sich in drei Bereiche: ACTIONS ist der wichtigste (s.u.); VARIABLES enthält Daten, die sich im Laufe einer Spiel-Session ändern können; MEDIA enthält Dateien, die wir einbinden können.
4. Damit auf der Bühne auch was passiert, müssen wir ACTIONS von links aus der Toolbar nach rechts auf die Stage ziehen. 
    
    <img src="./assets/actions1-1.png" height=400 alt="screenshot"/>

    Wir fangen mit etwas Einfachem an: Scroll in der Toolbar unter ACTIONS runter, bis du unter TELEGRAM **sendMessage** siehst. Dann zieh mit der Maus (drag and drop) die sendMessage ACTION auf einen freien Bereich der Stage.
     
     <img src="./assets/actions3-1.png" alt="screenshot"/>

    Jetzt kannst du den neu entstandenen State umbenennen. Wir nennen ihn mal WILLKOMMEN:
     
     <img src="./assets/actions4-2.png" alt="screenshot"/>

    STATES lassen sich auf der Stage via drag and drop bewegen: Einfach mit dem Mauszeiger über der Kopfzeile hovern, klicken und dann bewegen.

5. Jetzt wollen wir daran arbeiten, dass in unserem Level auch was passiert. Dazu müssen wir den START state mit unserem neuen state WILLKOMMEN verbinden. 

    Dazu öffnen wir die NEXT ACTION, die im START state schon bereit liegt, indem wir auf die Kopfzeile der NEXT ACTION klicken. Nun können wir unter "next state" in das Textfeld den Namen des state schreiben, der auf den START state folgen soll – in unserem Fall also "WILLKOMMEN".(Alternativ kann der state auch aus dem Drop Down Menu ausgewählt werden.) Danach klicken wir einmal irgendwo auf die Stage – und fertig. Nun geht es für Spieler:innen vom START sofort in unseren neuen state WILLKOMMEN:

     <img src="./assets/actions6-1.png" alt="screenshot"/><br>
     <img src="./assets/actions7-1.png" alt="screenshot"/>

6. In WILLKOMMEN wollen wir jetzt eine Nachricht an die Spieler:in schicken, die unser Level gestartet hat. 
    Dazu öffnen wir die **sendMessage** ACTION und füllen das Formular wie folgt aus:

     <img src="./assets/actions8-1.png" width=40% alt="screenshot"/>
     <img src="./assets/actions9-1.png" width=50% alt="screenshot"/>

    Der TELEGRAM CLIENT ist gewissermaßen unsere Figur, der Absender unserer Nachrichten. In unserem Workshop-Level gibt es nur eine Figur, aber in komplexeren Games könnten wir auch mehrere Charaktere haben, die mit unseren Spieler:innen kommunizieren.
    
    Unter "to" wählen wir "**Player**" aus. So entscheiden wir, an wen die Telegram-Nachricht gesendet wird. Auch hier haben wir nur eine Spieler:in zur Auswahl. Es könnten aber auch mehrere sein, z.B. eine Gruppe.

    Unter "**text**" schreiben wir einen kleinen Text (den Inhalt der Nachricht) und speichern das Ganze.

7. Jetzt öffnen wir noch die **next** ACTION und tragen QUIT unter next ein und schwupp! Unser Level sollte jetzt schon funktionieren! 
     <img src="./assets/actions10-1.png" width=30% alt="screenshot"/>
     
     So sollte das jetzt ungefähr aussehen:

     <img src="./assets/actions11-1.png" alt="screenshot"/>

<div class="page"/>

8. Jetzt sollten wir mal testen, ob alles funktioniert. Dazu schicken wir den Namen unseres Levels als Telegram-Nachricht an Thekla, unsere Testfigur:
Tipp: Die Nummer von Thekla findest du unter Game > Settings > Telegram > Client

     <img src="./assets/telegram1.png" alt="screenshot" height=400/>

    Wenn alles geklappt hat, sollte Thekla antworten: Hallo, wie heißt du?
    (Die Weiterleitung ist für unseren Workshop in einem übergeordneten Menu Level vorbereitet, deshalb bekommst du noch eine weitere Antwort zurück - die kannst du erstmal ignorieren.)



### Testing mit Session Ansicht

Wenn es Probleme gibt, klicke am besten als Erstes oben auf das kleine Dreieck in der rechten Ecke. Damit öffnest du die SESSION Ansicht.
Wenn eine Session im Gange ist, oder auch wenn etwas kaputt gegangen ist oder feststeckt, siehst du die Session dort oben und kannst sie manuell beenden, indem du auf den Mülleimer klickst.
Wenn du gerade keine Session sehen kannst, ist das ganz normal und liegt daran, dass diese bereits wieder beendet ist. Sende doch einfach noch einmal den Levelnamen an Thekla, um die Session zu sehen, während sie aktiv ist. 

Vergiss aber nicht, danach die Session Ansicht zu schließen!

<div class="page"/>

### User-Eingaben verwerten

Nun passiert erstmal nichts weiter in unserem Level. Es startet, die Spieler:in bekommt unsere Nachricht und dann endet es auch schon. 
Um Interaktion hinzuzufügen benutzen wir eine neue ACTION in einem neuen state:

Wir ziehen aus den TELEGRAM ACTIONS in der Toolbar eine DIALOG ACTION auf einen leeren Teil der Stage.


Wir wollen nun den Inhalt der Nachricht prüfen, die die Spieler:in an Thekla sendet, wenn sie an der ACTION dialog ankommt. Die Action dialog macht genau das: Das System wartet auf eine Eingabe der Spieler:in und überprüft dann nach von uns zu definierenden Regeln, wie es mit der Antwort der Spieler:in umgehen soll.


1. Benenne den neuen State, wie du willst, zum Beispiel "WAITING" (so nennen wir ihn in diesem Tutorial ab jetzt), und fülle ihn wie folgt aus:

2. Klicke in der DIALOG ACTION auf "Settings" und wähle "else" aus. Dann klicke zum Schließen wieder auf "Settings". ("if" kommt später, keine Sorge)

    <img src="./assets/dialog1-1.png" alt="screenshot" height=300/><br>

3. Wir sehen nun, dass sich ein else-Block in der ACTION zeigt. Dazu kommen wir gleich.

    Außerdem sehen wir dass, wenn wir in der Toolbar auf VARIABLES klicken, neue VARIABLEN aufgetaucht sind: Es gibt eine ganze Reihe von Boxen, deren übergeordnete Box so wie dein dialog-STATE benannt ist. In unserem Tutorial heißt sie "WAITING", darunter steht "dialog-1". (Eventuell musst du die Seite neu laden, um die neuen Variablen zu sehen.)

    <img src="./assets/dialog2-1.png" alt="screenshot" height="400px"/>
    <br>

    Hier finden wir ab sofort die Daten (z.B. die Eingaben der Spieler:innen), die uns aus dem State WAITING zur Verfügung stehen (natürlich immer erst dann, wenn das Level an dem STATE namens "WAITING" angekommen ist).
    
    <img src="./assets/dialog6-1.png" alt="screenshot"/>
    
4.  Wir haben die Spieler:in nach ihrem Namen gefragt und wollen sie ab sofort auch korrekt ansprechen, also benutzen wir den Antworttext, den die Spieler:in an unseren Client Thekla gesendet hat: Wir ziehen mit der Maus die VARIABLE **text** aus dem "dialog_1" in das Textfeld "**response**" des else-Blocks, wie im oberen Bild zu sehen.

    Im Textfeld taucht jetzt [[state.WAITING.dialog_1.text]] in eckigen Klammern auf. Die eckigen Klammern markieren Variablen und ähnliche Daten. (Wir könnten natürlich auch einfach selbst die Variable in das Textfeld tippen, aber drag & drop bewahrt uns vor Tippfehlern.)

<div class="page"/>

5. Schreibe nun einen Grußtext, zum Beispiel "Hallo" vor und/oder hinter die Variable.

    <img src="./assets/dialog3-1.png" alt="screenshot" height=400/>

    Alles innerhalb der eckigen Klammern wird im Spiel ersetzt. Alles ohne eckige Klammern wird genau so als Antwort (**response**) geschickt, wie es im Textfeld steht.

6. Trage nun unter **else** > **next state** QUIT ein, damit das Level auch automatisch beendet wird.
    Du kannst jetzt auch endlich wieder testen: Thekla sollte jetzt, wenn du ihr bei Telegram eine Nachricht schreibst, wie folgt antworten:

    <img src="./assets/telegram2.png" alt="screenshot" width=50%/>    

    Wie du siehst, speichert das Level deine Eingabe, wenn du am state "WAITING" ankommst, und kombiniert den Text dann im response. Yay!

7. Um nur auf bestimmte Eingaben und nicht einfach auf jede denkbare Eingabe zu reagieren, brauchen wir neben der "else"-Bedingung noch eine "if"-Bedingung: Öffne die **dialog_1 ACTION** innerhalb des states "WAITING". Nun klicke wieder auf "**Settings**" vom **Telegram client** und füge "**if**" hinzu:

    <img src="./assets/dialog5-1.png" alt="screenshot" width=50%/>

8. Jetzt taucht innerhalb der ACTION oberhalb des else-Blocks ein if-Block auf.
    Wähle im oberen Drop-Down-Menü "**Contains**", wenn es nicht schon ausgewählt ist. Nun kannst du darunter im contains-Block im Textfeld eingeben, was das magische Zauberwort sein soll, um Thekla zum Schweigen zu bringen. Zum Beispiel "ruhe".
    Und unter "next state" trägst du ein, welcher State eintreten soll, wenn die eingehende Nachricht "ruhe" beinhaltet . Zum Beispiel QUIT.

    Trag unter **respond** den Text ein, mit dem Thekla antworten soll, bevor der next state eintritt, zum Beispiel "ok. ich gebe Ruhe!". 

    <img src="./assets/dialog4-1.png" alt="screenshot" width=40%/>

    Jetzt hast du einen Filter mit if-else im dialog gebaut:
    Wenn du Thekla nun irgendeinen Text schickst, der die Buchstabenfolge "ruhe" enthält (if), wird sie antworten: "ok. ich gebe Ruhe!", auf alle anderen Antworten (else) wird Thekla antworten wie schon zuvor.
    Du kannst so viele if-conditions hinzufügen, wie du willst, und auf diese Weise riesige interaktive Erzählungen bauen.

### weitere Telegram Spezialfunktionen

Jetzt haben wir die Grundfunktionen und das Interface von adaptor:ex kennengelernt und unser erstes Telegram-Level mit den Telegram ACTIONS "sendMessage" und "dialog" gebaut.
In der Toolbar (links) unter ACTIONS finden sich aber noch weitere Telegram ACTIONS mit Spezialfunktionen:

1. **sendFile**

    sendFile kann Bilder, Sounddateien und Dokumente verschicken. Es funktioniert ähnlich wie sendMessage.
    Unter file können wir Dateien aus der Toolbar > MEDIA droppen  - oder wir können die URL zu Dateien, die im Internet erreichbar sind, verlinken, indem wir die Webadresse (http:// oder https://) eingeben. (Zum Beispiel Fotos von https://workshopics.imgbb.com/ deren Link wir mit Rechtsklick > Linkadresse kopieren können).
    
2. **sendGeo**

    sendGeo funktioniert ähnlich, braucht aber die Felder latitude (Breitengrad) und longitude (Längengrad) ausgefüllt (ACHTUNG: type muss Number sein). Damit schicken wir eine digitale Straßenkarte mit Markierung. Das kann sehr nützlich sein für Shows/Games, die die Spieler:innen an verschiedene Orte in der Stadt führen.

Es gibt noch viele weitere ACTIONS und Spezialfunktionen, wir können auch unsere eigenen Variablen definieren und später mit [[meineVariable]] in eckigen Klammern abfragen und und und...

------------

## weiterführende Infos

Mehr Infos erhaltet ihr unter:

https://www.machinacommons.world
oder unter

info@machinaex.de
www.machinaex.de


Da adaptor:ex bis Ende 2021 noch in Entwicklung ist, ändern sich Dinge noch ein wenig. Die Grundfunktionen und Bedienung werden aber dieselben bleiben und sich nur noch erweitern. Und da es ein open source Projekt ist, mit dem wir in Zukunft auch weiter arbeiten werden, wird das auch so bleiben. 

adaptor:ex kann außer Telegram auch noch viel mehr: Bühnenlicht kontrollieren, interaktive Requisiten ansteuern, mit anderer Software (Lichtprogrammen, Soundprogrammen, etc.) kommunizieren uvm. 
